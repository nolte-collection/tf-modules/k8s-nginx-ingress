
variable "namespace" {

}
variable "lp_ip" {

}

locals {
  EXTRA_VALUES = {
    controller = {
      admissionWebhooks = {
        patch = {
          image = {
            tag = "arm-v1.3.0"
          }
        }
      }
      service = {
        externalIPs = [var.lp_ip]
        annotations = {
          "metallb.universe.tf/address-pool" = "generic-cluster-pool"
        }
      }
    }
  }
}
# https://github.com/helm/charts/tree/master/stable/nginx-ingress
# https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx
resource "helm_release" "release" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES)
  ]
  set {
    name  = "controller.publishService.enabled"
    value = "true"
  }

  #set {
  #  name  = "controller.image.tag"
  #  value = "quay.io/kubernetes-ingress-controller/nginx-ingress-controller-arm:dev"
  #}
  #set {
  #  name  = "controller.image.tag"
  #  value = "gcr.io/google_containers/defaultbackend-arm:1.5"
  #}

}
